enum HTTPClientMethod {
    Get = "GET",
    Post = "POST"
}

enum HTTPClientRequestStatus {
    Unset = 0,
    Opened = 1,
    HeaderRecieved = 2,
    Loading = 3,
    Done = 4,
}

class HttpClient {
    protected _xmlhttp = new XMLHttpRequest();
    private _timeoutMilliSeconds:number = 1000 * 60 * 3;

    public setTimeoutMilliSeconds(milliSeconds: number):HttpClient {
        this._timeoutMilliSeconds = milliSeconds;
        return this;
    }
    public request(
        url:string,
        method:HTTPClientMethod,
        params:any = {},
        callback: (response: string) => void,
        onFailed: () => void = ()=>{},
        atob:boolean = false
    ) {
        let xmlhttp = this._xmlhttp;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == HTTPClientRequestStatus.Done) {
                let response = xmlhttp.response;
                if (atob) response = window.atob(response);
                if (xmlhttp.status == 200) {
                    callback(response);
                } else {
                    onFailed();
                }
            }
        }

        xmlhttp.open(method, url, true);
        xmlhttp.withCredentials = true;
        xmlhttp.timeout = this._timeoutMilliSeconds;
        xmlhttp.ontimeout = function(e) {
            onFailed();
        }
        xmlhttp.send(JSON.stringify(params));
    }

    public isRequested() {
        return this._xmlhttp.readyState >= HTTPClientRequestStatus.Opened;
    }
    public isDone() {
        return this._xmlhttp.readyState >= HTTPClientRequestStatus.Done;
    }
}
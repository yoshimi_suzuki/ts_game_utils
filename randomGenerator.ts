﻿class RandomGenerator {
    private x = 123456789;
    private y = 362436069;
    private z = 521288629;
    private w = 1;

    public initialize(seed:number) {
        this.x = 123456789;
        this.y = 362436069;
        this.z = 521288629;
        this.w = seed;
    }
 
    public reset() {
        this.initialize(this.w);
    }

    public next() {
        return this.xOrShift();
    }

    public get0To1() {
        let pow = Es6Utils.MAX_SAFE_INTEGER;
        return this.rangeInt(0, pow) / pow;
    }

    public range(min:number, max:number) {
        if (Es6Utils.isInteger(min)
            && Es6Utils.isInteger(max))
            return this.rangeInt(min, max);
        let ratio = this.get0To1();
        let length = max - min;
        return min + length * ratio;
    }

    // excludes max
    public rangeInt(min:number, max:number) {
        if (min == max
            || min > max)
            return min;
        let next = this.next();
        let length = max - min;
        return min + (Math.abs(next)%length);
    }

    private xOrShift() {
        let t;
        t = this.x ^ (this.x << 11);
        this.x = this.y; this.y = this.z; this.z = this.w;
        return this.w = (this.w ^ (this.w >>> 19)) ^ (t ^ (t >>> 8)); 
    }
}


﻿class KeyFrameAnimation {

    private _currentFrame: number = 0;
    private _duration: number = 0;
    private _loopFrame: number = -1;

    private _positions: Array<KeyFrame<Atagoal.Position>> = new Array<KeyFrame<Atagoal.Position>>();
    private _rotations: Array<KeyFrame<number>> = new Array<KeyFrame<number>>();
    private _scales: Array<KeyFrame<KeyFrameScale>> = new Array<KeyFrame<KeyFrameScale>>();
    private _colors: Array<KeyFrame<KeyFrameColor>> = new Array<KeyFrame<KeyFrameColor>>();
    private _events: Array<KeyFrame<string>> = new Array<KeyFrame<string>>();
    

    public tick() {
        if (this.isFinished()) {
            return;
        }

        this._currentFrame++;

        if (this._loopFrame >= 0
            && this._currentFrame > this._duration)
            this._currentFrame = this._loopFrame;
    }

    public get position(): Atagoal.Position {
        if (this._positions.length == 0)
            return null;

        let result: Atagoal.Position = new Atagoal.Position();

        this.getFrameValue<Atagoal.Position>(
            this._positions,
            this._currentFrame,
            (
                last,
                next,
                ratio
            )=>{
                if (last == null
                    || next == null)
                    return;

                result = last.value.getMovedPosition(
                    Atagoal.Vector.createFromPoints(last.value, next.value).scale(ratio)
                );
            }
        );

        return result;
    }
    
    // 0 - 360
    public get rotation(): number {
        if (this._rotations.length == 0)
            return null;

        let result: number = 0;

        this.getFrameValue<number>(
            this._rotations,
            this._currentFrame,
            (
                last,
                next,
                ratio
            )=>{
                if (last == null
                    || next == null)
                    return;

                let velocity = next.value - last.value;
                result =  last.value + velocity * ratio;
            }
        );

        return result;
    }

    public get scale(): KeyFrameScale {
        if (this._scales.length == 0)
            return null;

        let result: KeyFrameScale = new KeyFrameScale(1, 1);

        this.getFrameValue<KeyFrameScale>(
            this._scales,
            this._currentFrame,
            (
                last,
                next,
                ratio
            )=>{
                if (last == null
                    || next == null)
                    return;

                let velocityX = next.value.x - last.value.x;
                let velocityY = next.value.y - last.value.y;

                result = new KeyFrameScale(
                    last.value.x + velocityX * ratio,
                    last.value.y + velocityY * ratio
                );
            }
        );

        return result;
    }

    public get color(): KeyFrameColor {
        if (this._colors.length == 0)
            return null;

        let result: KeyFrameColor = new KeyFrameColor(1,1,1,1);

        this.getFrameValue<KeyFrameColor>(
            this._colors,
            this._currentFrame,
            (
                last,
                next,
                ratio
            )=>{
                if (last == null
                    || next == null)
                    return;

                let velocityR = next.value.r - last.value.r;
                let velocityG = next.value.g - last.value.g;
                let velocityB = next.value.b - last.value.b;
                let velocityA = next.value.a - last.value.a;

                let results = new Array<number>(
                    last.value.r + velocityR * ratio,
                    last.value.g + velocityG * ratio,
                    last.value.b + velocityB * ratio,
                    last.value.a + velocityA * ratio
                );
                for (let i = 0; i < results.length; i++) {
                    if (results[i] > 1)
                        results[i] = 1;
                    if (results[i] < 0)
                        results[i] = 0;
                }
                
                result = new KeyFrameColor(
                    results[0],
                    results[1],
                    results[2],
                    results[3]
                );
            }
        );

        return result;
    }

    public get eventMesage() {
        let result: string = "";

        this.getFrameValue<string>(
            this._events,
            this._currentFrame,
            (
                last,
                next,
                ratio
            ) => {
                if (last == null)
                    return;
                if (last.frame != this._currentFrame)
                    return;

                result = last.value;
            }
        );

        return result;
    }
    
    

    public isFinished() {
        return this._loopFrame < 0
            && this._currentFrame >= this._duration;
    }


    // without sort
    public addPositionKeyFrame(
        frame: number,
        position: Atagoal.Position
    ) {
        this._positions.push(
            new KeyFrame<Atagoal.Position>(frame, position)
        );
    }

    // without sort
    public addRotationKeyFrame(
        frame: number,
        rotation: number
    ) {
        this._rotations.push(
            new KeyFrame<number>(
                frame,
                rotation
            )
        );
    }

    // without sort
    public addScaleKeyFrame(
        frame: number,
        x: number,
        y: number 
    ) {
        this._scales.push(
            new KeyFrame<KeyFrameScale>(
                frame,
                new KeyFrameScale(x, y)
            )
        );
    }

    // without sort
    public addColorKeyFrame(
        frame: number,
        r: number,
        g: number,
        b: number,
        a: number
    ) {
        this._colors.push(
            new KeyFrame<KeyFrameColor>(
                frame,
                new KeyFrameColor(
                    r,
                    g,
                    b,
                    a
                )
            )
        );
    }

    // without sort
    public addEventKeyFrame(
        frame: number,
        message: string
    ) {
        this._events.push(
            new KeyFrame<string>(
                frame,
                message
            )
        );
    }
    
    

    // -------------------------- setting up
    /*
        setting up with JSON string as bellow.

        for exsample,
        {
          "duration": 240,
          "loop": 120,
          "positions": [
            {
              "frame": 0,
              "x": 40,
              "y": 200
            },
            {
              "frame": 120,
              "x": 240,
              "y": 200
            },
            {
              "frame": 180,
              "x": 240,
              "y": 400
            }
          ],
          "rotations": [
            {
              "frame": 0,
              "rotation": 0
            },
            {
              "frame": 120,
              "rotation": 60
            },
            {
              "frame": 240,
              "rotation": -60
            }
          ],
          "scales": [
            {
              "frame": 0,
              "x": 1,
              "y": 1
            },
            {
              "frame": 120,
              "x": 1,
              "y": 1
            },
            {
              "frame": 240,
              "x": 0,
              "y": 0
            }
          ],
          "colors": [
            {
              "frame": 0,
              "r": 1,
              "g": 1,
              "b": 1,
              "a": 1
            },
            {
              "frame": 120,
              "r": 1,
              "g": 0,
              "b": 0,
              "a": 1
            },
            {
              "frame": 240,
              "r": 1,
              "g": 1,
              "b": 1,
              "a": 0
            }
          ],
          "events": [
            {
              "frame": 120,
              "message": "exclusive data"
            }
          ]
        }
    **/
    public setJsonData(json: Object) {
        if (json.hasOwnProperty("positions"))
            this.setPositionData(json["positions"]);
        if (json.hasOwnProperty("rotations"))
            this.setRotationData(json["rotations"]);
        if (json.hasOwnProperty("scales"))
            this.setScaleData(json["scales"]);
        if (json.hasOwnProperty("colors"))
            this.setColorData(json["colors"]);
        
        this._duration = json["duration"];
        this._loopFrame = json.hasOwnProperty("colors") 
            ? json["loop"]
            : -1;

        return this;
    }

    private setPositionData(json: Array<Object>) {
        for (let keyFrame of json) {
            this.addPositionKeyFrame(
                keyFrame["frame"],
                new Atagoal.Position(
                    keyFrame["x"],
                    keyFrame["y"]
                )
            );
        }

        let firstFrame: KeyFrame<Atagoal.Position> = null;
        for (let keyFrame of this._positions) {
            if (keyFrame.frame == 0)
            {
                firstFrame = keyFrame.clone();
                break;
            }
        }
        if (firstFrame == null) {
            this._positions.push(new KeyFrame(0, new Atagoal.Position(0, 0)));
        }
    }
    private setRotationData(json: Array<Object>) {
        for (let keyFrame of json) {
            this.addRotationKeyFrame(
                keyFrame["frame"],
                keyFrame["rotation"]
            );
        }

        let firstFrame: KeyFrame<number> = null;
        for (let keyFrame of this._rotations) {
            if (keyFrame.frame == 0)
            {
                firstFrame = keyFrame.clone();
                break;
            }
        }
        if (firstFrame == null) {
            this.addRotationKeyFrame(
                0,
                0
            );
        }
    }
    private setScaleData(json: Array<Object>) {
        for (let keyFrame of json) {
            this.addScaleKeyFrame(
                keyFrame["frame"],
                keyFrame["x"],
                keyFrame["y"]
            );
        }

        let firstFrame: KeyFrame<KeyFrameScale> = null;
        for (let keyFrame of this._scales) {
            if (keyFrame.frame == 0)
            {
                firstFrame = keyFrame.clone();
                break;
            }
        }
        if (firstFrame == null) {
            this.addScaleKeyFrame(
                0,
                1,
                1
            );
        }
    }
    private setColorData(json: Array<Object>) {
        for (let keyFrame of json) {
            this.addColorKeyFrame(
                keyFrame["frame"],
                keyFrame["r"],
                keyFrame["g"],
                keyFrame["b"],
                keyFrame["a"]
            );
        }

        let firstFrame: KeyFrame<KeyFrameColor> = null;
        for (let keyFrame of this._colors) {
            if (keyFrame.frame == 0) {
                firstFrame = keyFrame.clone();
                break;
            }
        }
        if (firstFrame == null) {
            this.addColorKeyFrame(
                0,
                1,
                1,
                1,
                1
            );
        }
    }
    private setEventData(json: Array<Object>) {
        for (let keyFrame of json) {
            this.addEventKeyFrame(
                keyFrame["frame"],
                keyFrame["message"]
            );
        }
    }

    

    // ------------------------------ utils
    private getFrameValue<T>(
        keyFrames: Array<KeyFrame<T>>,
        frame: number,
        callback: (
            last: KeyFrame<T>,
            next: KeyFrame<T>,
            ratio: number
        ) => void
    ) {
        let last:KeyFrame<T> = null;
        let next:KeyFrame<T> = null;

        for(let keyFrame of keyFrames) {
            if (keyFrame.frame == frame)
            {
                callback(
                    keyFrame.clone(),
                    keyFrame.clone(),
                    0
                )
                return;
            }

            if (keyFrame.frame < frame)
            {
                last = keyFrame.clone();
                continue;
            }

            if (keyFrame.frame > frame)
            {
                next = keyFrame.clone();
                break;
            }
        }

        if (last == null) {
            callback(
                null,
                null,
                0
            );
            return;
        }

        if (next == null) {
            callback(
                last,
                last,
                0
            );
            return;
        }

        let diff = next.frame - last.frame;
        let ratio = (this._currentFrame - last.frame) / diff;
    
        callback(
            last,
            next,
            ratio
        );
    }
}

class KeyFrame<T> {
    constructor(
        public frame: number,
        public value: T
    ){}

    public clone() {
        return new KeyFrame<T>(this.frame, this.value);
    }
}

class KeyFrameScale {
    constructor(
        public x: number,
        public y: number
    ){}    
}

class KeyFrameColor {
    constructor(
        public r: number,
        public g: number,
        public b: number,
        public a: number
    ){}    
}
﻿class SerialProcess {
    private _processes:Queue<(finish:()=>void)=>void> = new Queue<(finish:()=>void)=>void>();

    public add(process:(finish:()=>void)=>void)
    {
        this._processes.enqueue(process);
        return this;
    }

    public flush()
    {
        this.next();
    }

    private next()
    {
        if (this._processes.isEmpty())
            return;

        let process = this._processes.dequeue();
        process(() =>
        {
            this.next();
        });
    }
}

class Queue<T> {
    private _elements: Array<T> = new Array<T>(); 
    public enqueue(element:T) {
        this._elements.push(element);
    }
    public dequeue():T {
        let element:T = this.isEmpty()
            ? null
            : this._elements[0];
        this.slide();
        return element;
    }
    public isEmpty() {
        return this._elements.length == 0;
    }

    private slide() {
        let newElements = new Array<T>();
        let length = this._elements.length;
        for (let i = 1; i < length; i++) {
            newElements.push(this._elements[i]); 
        }
        this._elements = newElements;
    }
}
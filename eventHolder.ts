﻿class EventHolder<T extends Function = VoidFunction> {
    private _callbacks:Array<T> = new Array<T>();
    public dispatch(
        arg1:any = null,
        arg2:any = null,
        arg3:any = null,
        arg4:any = null,
        arg5:any = null) {
        for (let callback of this._callbacks)
            callback(arg1,arg2,arg3,arg4,arg5);
    }

    public add(callback:T) {
        this._callbacks.push(callback);
    }

    public remove(callback:T) {
        let index = this._callbacks.indexOf(callback);
        if (index < 0)
            return;
        this._callbacks.splice(index, 1);
    }

    public reset() {
        this._callbacks = new Array<T>();
    }
}
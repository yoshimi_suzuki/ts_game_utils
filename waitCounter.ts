﻿class WaitCounter {
    constructor(
        protected limit: number,
        protected onFinished: () => void) {

    }

    protected elaspedFrame: number = 0;

    public tick() {
        if (this.isFinished()) {
            return;
        }

        this.elaspedFrame++;

        if (this.isFinished()) {
            this.onFinished();
        }
    }

    protected isFinished(): boolean {
        return this.elaspedFrame > this.limit;
    }

    public get ratio() {
        let ratio = this.elaspedFrame / this.limit;
        return ratio > 1 ? 1 : ratio;
    }

    public seek(elasped:number) {
        this.elaspedFrame = elasped;
        return this;
    }
}
﻿class Es6Utils {
    public static endsWith(
        target:string,
        search:string,
        len:number = undefined
    ) {
        if (len === undefined || len > target.length) {
            len = target.length;
        }
        return target.substring(len - search.length, len) === search;
    }

    public static isInteger(
        target: number
    ) {
        return typeof target === "number" && 
            isFinite(target) && 
            Math.floor(target) === target;
    }

    public static get MAX_SAFE_INTEGER() {
        return Math.pow(2, 53) - 1;
    }
}
﻿class Profiler {
    private static internalInstance: Profiler = null;
    public static get instance() {

        if (Profiler.internalInstance == null)
        Profiler.internalInstance = new Profiler();

        return Profiler.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (Profiler.internalInstance != null)
            console.error("The constructor of Profiler has been called by outside. Do not new() a Singleton Class directly.");
    }

    private _isActive:boolean = false;
    private _startTime:number = 0;
    private _points:Array<PlofilePoint> = new Array<PlofilePoint>();

    public activate() {
        this._isActive = true;
    }

    public start() {
        this._points = new Array<PlofilePoint>();
        this._startTime = Date.now();
    }

    public check(tag:string) {
        if (!this._isActive) return;
        this._points.push(new PlofilePoint(tag, Date.now() - this._startTime));
    }

    public end() {
        if (!this._isActive) return 0;
        let endTime = Date.now() - this._startTime;

        for(let point of this._points) {
            console.log(point.tag+ " : " + point.time + " ms");
        }
        console.log("end : " + endTime + " ms");
    }
}

class PlofilePoint {
    constructor(
        public tag:string,
        public time:number
    ){}
}
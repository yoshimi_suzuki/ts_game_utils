﻿class PetitSynchronizer {
    public constructor(
        tags:Array<string>,
        callback:()=>void,
        private _isEndless:boolean = false
    ){
        for (let tag of tags) {
            this._flags.register(tag, false);
        }
        this._callback = callback;
    }

    private _flags:KVSMap<boolean> = new KVSMap<boolean>();
    private _callback:()=>void = null;

    public finish(tag:string) {
        if (this._flags.get(tag) == null) return;
        this._flags.register(tag, true);
        this.checkFinished();
    }

    private checkFinished() {
        if (this._callback == null) return;
        for (let key of this._flags.getKeys()) {
            if (!this._flags.get(key)) return;
        }
        this._callback();

        if (!this._isEndless) {
            this._callback = null;
        }
    }
}
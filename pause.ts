﻿class Pause {
    private static internalInstance: Pause = null;
    public static get instance() {

        if (Pause.internalInstance == null)
            Pause.internalInstance = new Pause();

        return Pause.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (Pause.internalInstance != null)
            console.error("The constructor of Pause was called by outside. Do not new() a Singleton Class directly.");
    }

    public onPaused: () => void = ()=>{};
    public onResumed: () => void = ()=>{};

    private _paused: boolean = false;

    public pause() {
        this._paused = true;
        this.onPaused();
    }

    public resume() {
        this._paused = false;
        this.onResumed();
    }

    public get paused() {
        return this._paused;
    }
}
﻿class ResolutionManager {
    private static internalInstance: ResolutionManager = null;
    public static get instance() {

        if (ResolutionManager.internalInstance == null)
            ResolutionManager.internalInstance = new ResolutionManager();

        return ResolutionManager.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (ResolutionManager.internalInstance != null)
            console.error("The constructor of ResolutionManager was called by outside. Do not new() a Singleton Class directly.");
    }

    private _width: number;
    private _height: number;

    private _scale: number;
    private _offset: Atagoal.Vector;

    public initialize(
            gameWidth: number,
            gameHeight: number,
            isFit: boolean = false) {
        let element = document.getElementById("content");
        element.style.position = "absolute";
        element.style.left = "0px";
        element.style.top = "0px";

        let gameW: number = gameWidth;
        let gameH: number = gameHeight;
        let windowW = window.innerWidth;
        let windowH = window.innerHeight;

        if (!isFit
            && windowW >= windowH) {
            this._width = gameW;
            this._height = gameH;
            this._scale = 1;
            this._offset = new Atagoal.Vector();
        } else {
            this._width = windowW;
            this._height = windowH;
            this._scale = this.width / gameW;
            this._offset = new Atagoal.Vector();
        }
    }

    public get width() {
        return this._width;
    }
    public get height() {
        return this._height;
    }
    public get scale() {
        return this._scale;
    }

    public convertPosition(position: Atagoal.Position) {
        return position
            .clone()
            .scale(this._scale)
            .move(this._offset);
    }

    public convertVector(vector: Atagoal.Vector) {
        return vector
            .clone()
            .scale(this._scale)
            .move(this._offset);
    }
}
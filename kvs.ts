﻿class KVSMap<T> {
    private keys: Array<string> = new Array<string>();
    private values: Array<T> = new Array<T>();

    public register(key: string, value: T) {
        let index = this.keys.indexOf(key);
        if (index >= 0) {
            this.values[index] = value;
            return;
        }

        this.keys.push(key);
        this.values.push(value);
    }

    public get(key: string) {
        let index = this.keys.indexOf(key);
        if (index >= 0) {
            return this.values[index];
        }

        return null;
    }

    public getValues() {
        return this.values;
    }
    
    public getKeys() {
        return this.keys;
    }
}
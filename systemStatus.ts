﻿class SystemStatus {
    private static internalInstance: SystemStatus = null;
    public static get instance() {

        if (SystemStatus.internalInstance == null)
            SystemStatus.internalInstance = new SystemStatus();

        return SystemStatus.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (SystemStatus.internalInstance != null)
            console.error("The constructor of SystemStatus was called by outside. Do not new() a Singleton Class directly.");
    }


    protected _isDebugMode: boolean = false;
    protected _isOnLine: boolean = false;

    protected static _onStatusUpdated: ()=>void = ()=>{};

    public initialize(callback: () => void) {
        // todo : サーバーからデータを取得するようにする

        this._isDebugMode = true;
        this._isOnLine = window.navigator.onLine;

        window.addEventListener("offline",
            function (e) {
                SystemStatus.instance.isOnLine = false;
                SystemStatus._onStatusUpdated();
            });

        window.addEventListener("online",
            function (e) {
                SystemStatus.instance.isOnLine = true;
                SystemStatus._onStatusUpdated();
            });

        callback();
    }

    public get isDebugMode() {
        return this._isDebugMode;
    }

    public set isOnLine(value: boolean) {
        this._isOnLine = value;
    }

    public get isOnLine() {
        return this._isOnLine;
    }

    public set onStatusUpdated(callback: () => void) {
        SystemStatus._onStatusUpdated = callback;
    }
}
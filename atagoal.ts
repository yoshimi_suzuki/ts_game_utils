﻿namespace Atagoal {
    /** The classes of Geometries */
    export class Position {
        public constructor(x: number = 0, y: number = 0) {
            this.x = x;
            this.y = y;
        }

        public x: number;
        public y: number;

        public move(vector: Vector) {
            this.x += vector.x;
            this.y += vector.y;
            return this;
        }

        public scale(ratio: number) {
            this.x *= ratio;
            this.y *= ratio;
            return this;
        }

        public getMovedPosition(vector: Vector) {
            return this.clone().move(vector);
        }
        public getMoved(x: number, y: number) {
            return this.getMovedPosition(new Vector(x, y));
        }

        public clone() {
            return new Position(this.x, this.y);
        }

        public describe(): string {
            return "(" + this.x + "," + this.y + ")";
        }
    }

    export class Vector {
        public constructor(x: number = 0, y: number = 0) {
            this.x = x;
            this.y = y;
        }

        public x: number;
        public y: number;

        public static createFromPoints(
            from: Position,
            to: Position) {
            return new Vector(
                to.x - from.x,
                to.y - from.y);
        }

        public move(vector: Vector) {
            this.x += vector.x;
            this.y += vector.y;
            return this;
        }

        public scale(ratio: number) {
            this.x *= ratio;
            this.y *= ratio;
            return this;
        }

        public getRotated(angle: number) {
            let radian = Math.PI * angle / 180;
            let polarVec = this.toPoalar();
            polarVec.radian += radian;
            return polarVec.toXY();
        }

        public getAngle(target: Vector = new Vector(0, -1)) {
            let radian = this.getRadian(target);
            return radian * 180.0 / Math.PI;
        }
        public getRadian(target: Vector = new Vector(0, -1)) {
            if (this.length * target.length == 0) {
                // todo : assert
                return 0;
            }

            let inner = Util.innerProduct(this, target) / (this.length * target.length);
            return Math.acos(inner);
        }


        public get length() {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        }

        public get unit() {
            return this.length != 0
                ? this.scale(1 / this.length)
                : this;
        }

        public toPoalar() {
            let radian = Math.atan2(this.y, this.x);
            return new PolarVector(radian, this.length);
        }

        public clone() {
            return new Vector(this.x, this.y);
        }

        public describe(): string {
            return "+(" + this.x + "," + this.y + ")";
        }
    }

    export class PolarVector {
        public constructor(radian: number = 0, length: number = 0) {
            this.radian = radian;
            this.length = length;
        }

        public radian: number;
        public length: number;

        public toXY() {
            return new Vector(
                this.length * Math.cos(this.radian),
                this.length * Math.sin(this.radian));
        }

        public clone() {
            return new PolarVector(this.radian, this.length);
        }
    }

    export class LineSegment {
        public constructor(
            public from: Position,
            public to: Position) {
        }

        public isCross(line: LineSegment): boolean {
            return this.intersection(line) != null;
        }

        public intersection(line: LineSegment): Position {
            return Atagoal.Util.intersection(this, line);
        }

        public getDistanceTo(target: Position) {
            let vector = this.toVector();
            let toTarget = Vector.createFromPoints(this.from, target);

            if (this.length == 0) {
                return 0;
            }

            return Math.abs(Util.crossProduct(vector, toTarget) / vector.length);
        }

        public get length() {
            return this.toVector().length;
        }

        public toVector() {
            return Vector.createFromPoints(this.from, this.to);
        }

        public clone() {
            return new LineSegment(this.from, this.to);
        }
    }

    export class Util {
        public static innerProduct(vector1: Vector, vector2: Vector) {
            if (vector1 == null
                || vector2 == null)
                return 0;

            return vector1.x * vector2.x + vector2.y * vector1.y;
        }

        public static crossProduct(vector1: Vector, vector2: Vector) {
            if (vector1 == null
                || vector2 == null)
                return 0;

            return vector1.x * vector2.y - vector2.x * vector1.y;
        }

        public static intersection(line1: LineSegment, line2: LineSegment) {
            let vector1 = new Vector(
                line1.to.x - line1.from.x,
                line1.to.y - line1.from.y);
            let vector2 = new Vector(
                line2.from.x - line1.from.x,
                line2.from.y - line1.from.y);
            let vector3 = new Vector(
                line2.to.x - line1.from.x,
                line2.to.y - line1.from.y);

            let vector4 = new Vector(
                line2.to.x - line2.from.x,
                line2.to.y - line2.from.y);
            let vector5 = new Vector(
                line1.from.x - line2.from.x,
                line1.from.y - line2.from.y);
            let vector6 = new Vector(
                line1.to.x - line2.from.x,
                line1.to.y - line2.from.y);

            let cross1 = Atagoal.Util.crossProduct(vector1, vector2);
            let cross2 = Atagoal.Util.crossProduct(vector1, vector3);

            let cross3 = Atagoal.Util.crossProduct(vector4, vector5);
            let cross4 = Atagoal.Util.crossProduct(vector4, vector6);

            // has no intersection (out of line segment.)
            if (cross1 * cross2 > 0
                || cross3 * cross4 > 0
                // has no intersection (line segments are parallel.)
                || (cross1 === 0 && cross2 === 0))
                return null;

            let ratio = Math.abs(cross1) / (Math.abs(cross1) + Math.abs(cross2));

            let intersection = new Position(
                line2.from.x + vector4.x * ratio,
                line2.from.y + vector4.y * ratio
            );

            // has an intersection point.
            if (0 <= ratio && ratio <= 1)
                return intersection;

            // has no intersection (out of line segment.)
            return null;
        }
    }
}

﻿class StringUtils {
    public static format(
        target:number,
        digit:number
    ) {
        let result = target.toString();
        while(result.length < digit) {
            result = "0" + result;
        }
        return result;
    }
}